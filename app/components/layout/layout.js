class Layout {
    constructor({rows, seats}, price, bookedTickets = [], ticketAddedObservable) {
        this.ticketAddedObservable = ticketAddedObservable;
        this.clickHandler = this.clickHandler.bind(this);
        this.render(rows, seats, price, bookedTickets);
        bookedTickets.map(ticket => this.reserveItem(ticket));
    };

    deselectItem({row, seat}) {
        document.querySelector('.seat[data-row="'+row+'"][data-seat="'+seat+'"]').classList.remove('seat--selected');
    };

    reserveItem({row, seat}) {
        document.querySelector('.seat[data-row="'+row+'"][data-seat="'+seat+'"]').classList.add('seat--reserved');
    };

    selectItem({row, seat}) {
        document.querySelector('.seat[data-row="'+row+'"][data-seat="'+seat+'"]').classList.add('seat--selected');
    };

    clickHandler(e) {
        if(e.target.classList.contains('seat--selected') || e.target.classList.contains('seat--reserved')) {
            return
        } else {
            e.target.classList.add('seat--selected');
            let data = {
                row: e.target.dataset.row,
                seat: e.target.dataset.seat,
                price: e.target.dataset.price
            };
            this.ticketAddedObservable.sendMessage(data);
        };
    };

    render(rows, seats, price) {
        let app = document.getElementById('app');
        let template = `
            <div class="screen"></div>`;

        let layout = document.createElement('div');
        layout.classList.add('layout');
        layout.innerHTML = template;

        for (let i = 0; i < rows; i++) {
            let rowDomEl = document.createElement('div');
            rowDomEl.classList.add('row');
            for (let j = 0; j < seats; j++) {
                let seatDomEl = document.createElement('div');
                seatDomEl.classList.add('seat');
                seatDomEl.dataset.row = i+1;
                seatDomEl.dataset.seat = j+1;
                if(i <= 2) {
                    seatDomEl.classList.add('price--low');
                    seatDomEl.dataset.price = price.low;
                } else if(i == rows - 1) {
                    seatDomEl.classList.add('price--high');
                    seatDomEl.dataset.price = price.high;
                } else {
                    seatDomEl.classList.add('price--medium');
                    seatDomEl.dataset.price = price.medium;
                };
                seatDomEl.onclick = this.clickHandler;
                rowDomEl.appendChild(seatDomEl);
            };
            layout.appendChild(rowDomEl);
        };

        app.appendChild(layout);
    };
};


export default Layout;