class Cart {
    constructor(ticketRemovedObservable, ticketPurchasedObservable) {
        this.ticketRemovedObservable = ticketRemovedObservable;
        this.ticketPurchasedObservable = ticketPurchasedObservable;
        this.tickets = [];
        this.removeItem = this.removeItem.bind(this);
        this.handleBuyButtonClick = this.handleBuyButtonClick.bind(this);
        this.render();
    };

    saveDataToLocalStorage() {
        let currentTickets = JSON.parse(localStorage.getItem('tickets')) || [];
        localStorage.setItem('tickets', JSON.stringify([...currentTickets, ...this.tickets]));
    };

    clearTimeouts(ticket) {
        clearTimeout(ticket.timeOut);
        delete ticket.timeOut;
    }

    handleBuyButtonClick() {
        this.tickets.map(ticket => {
            this.clearTimeouts(ticket);
            this.ticketPurchasedObservable.sendMessage(ticket);
        });

        this.saveDataToLocalStorage();

        let order = 'Спасибо за покупку! \nВаш заказ! \n';
        this.tickets.forEach( (ticket, i) => {
            order += `Билет ${i+1}: Ряд ${ticket.row}, место ${ticket.seat}, цена ${ticket.price}. \n`
        });
        order += `Всего: ${this.tickets.reduce( (prev, current) => {
            return prev += Number(current.price);
          }, 0)} грн`;
        alert(order);

        this.tickets = [];
        this.render();
    };


    removeItem(row, seat) {
        let index = this.tickets.findIndex(ticket => ticket.row == row && ticket.seat == seat);
        this.clearTimeouts(this.tickets[index]);
        this.ticketRemovedObservable.sendMessage(this.tickets[index]);
        this.tickets.splice(index, 1);
        document.querySelector('.cart_item[data-row="'+row+'"][data-seat="'+seat+'"]').remove();

        this.render();
    };

    addTicket(ticket) {
        let timeOut = setTimeout(() => this.removeItem(ticket.row, ticket.seat), 60000);
        ticket.timeOut = timeOut;
        this.tickets.push(ticket);

        this.render();
    };

    render() {
        let app = document.getElementById('app');
        let cart = document.getElementById('cart');

        if(cart) {
            cart.remove();
        }

        cart = document.createElement('div');
        cart.classList.add('cart');
        cart.setAttribute('id', 'cart');

        let messageNode = document.createElement('div');
        let message;
        if( this.tickets.length === 0 ){
          message = 'У вас в корзине пусто. Выберите места на схеме.';
          messageNode.innerHTML = message;
        } else {
          let total = this.tickets.reduce( (prev, current) => {
            return prev += Number(current.price);
          }, 0);
          message = `<p>У вас в корзине ${this.tickets.length} товаров, на сумму: ${total} грн.</p><button id="butBtn">Купить</button>`;
          messageNode.innerHTML = message;
          messageNode.querySelector('#butBtn').onclick = this.handleBuyButtonClick;
        }
        
        cart.appendChild(messageNode);

        this.tickets.map(ticket => {
            let template = `
                <span class='row'>${ticket.row} <br>ряд</span>
                <span class='seat'>${ticket.seat} <br>место</span>
                <span class='price'>${ticket.price} грн</span>
                <div class='delete'>x</div>`;

            let item = document.createElement('div');
            item.classList.add('cart_item');
            item.dataset.row = ticket.row;
            item.dataset.seat = ticket.seat;
            item.innerHTML = template;
            item.querySelector('.delete').onclick = () => this.removeItem(ticket.row, ticket.seat);

            cart.prepend(item);
        })

        app.appendChild(cart);
    };
};

export default Cart;