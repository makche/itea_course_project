import { Observable, Observer } from './components/observer/observer';
import Layout from './components/layout/layout';
import Cart from './components/cart/cart';


document.addEventListener("DOMContentLoaded", function() {

    let layoutConfig = {
        rows: 8,
        seats: 12,
    };

    let priceConfig = {
        low: '85',
        medium: '100',
        high: '125'
    };

    class Cinema {
        constructor(name, layoutConfig, priceConfig) {
            this.name = name;
            this.cart = null;
            this.layout = null;
            this.tickets = this.getDataFromLocalStorage();;
            this.ticketAddedObservable = new Observable();
            this.ticketRemovedObservable = new Observable();
            this.ticketPurchasedObservable = new Observable();
            this.initApp(layoutConfig, priceConfig);
            this.initObservers();
        };

        getDataFromLocalStorage() {
            return JSON.parse(localStorage.getItem('tickets')) || [];
        };


        initApp(layoutConfig, priceConfig) {
            this.layout = new Layout(layoutConfig, priceConfig, this.tickets, this.ticketAddedObservable);
            this.cart = new Cart(this.ticketRemovedObservable, this.ticketPurchasedObservable);
        };

        initObservers() {
            let addToCartObserver = new Observer( data => this.cart.addTicket(data));
            this.ticketAddedObservable.addObserver(addToCartObserver);

            let removeFromCartObserver = new Observer( data => this.layout.deselectItem(data));
            this.ticketRemovedObservable.addObserver(removeFromCartObserver);

            let ticketPurchasedObserver = new Observer( data => this.layout.reserveItem(data));
            this.ticketPurchasedObservable.addObserver(ticketPurchasedObserver);
        };
    };

    const multiPlex = new Cinema('Multiplex', layoutConfig, priceConfig);
    console.log(multiPlex);

});